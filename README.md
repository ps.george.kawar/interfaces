**You can use any class or Third Party Library Classes to get the date or time**

**you are NOT allowed to use Formatting classes other than the ones you will create**
___
**1)** Use the SpecialDateFormatter Interface with implementing the following:
    
a) Get any day date in a dd/MM/yyyy pattern, example: 
> Output:
>
> 07/05/2021
    
b) Get any day date in a dd of MMMM yyyy (Sunday-Saturday) pattern, example: 
> Output:
>
> 7th of May 2021 (Friday)

___
**2)** Create a SpecialTimeFormatter Interface with the method - String format(String time); -and implement the following:

a) Get any time in a day: hours:minutes.seconds pattern, example:
> Output:
>
>Monday: 05:01.40 


# **Optional**
Create an Interface that will be used in Classes to format any string if a pattern is specified, example:

Formatter.format("7th of May 2021 (Friday)", "MM/dd/yyyy")
> Output:
>
> 05/07/2021

or
Formatter.format("7th of May 2021 (Friday)", "yyyyddMM")
> Output:
>
> 20210705
