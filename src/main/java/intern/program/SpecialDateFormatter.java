package intern.program;

public interface SpecialDateFormatter {

    String format(String date);

}
